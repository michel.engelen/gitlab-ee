export const WARNING = 'warning';
export const DANGER = 'danger';

export const WARNING_ALERT_CLASS = 'warning_message';
export const DANGER_ALERT_CLASS = 'danger_message';

export const WARNING_TEXT_CLASS = 'text-warning-900';
export const DANGER_TEXT_CLASS = 'text-danger-900';
